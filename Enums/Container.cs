namespace vcs.Enums
{
    public enum Container 
    {
        AVI = 0,
        MKV = 1,
        MP4 = 2,
        WEBM = 3
    }
}