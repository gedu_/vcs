namespace vcs.Enums
{
    public enum AudioCodec 
    {
        AAC = 0,
        AC3 = 1,
        DTS = 2,
        MP3 = 3,
        OPUS = 4,
        VORBIS = 5
    }
}