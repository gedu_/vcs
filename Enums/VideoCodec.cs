namespace vcs.Enums
{
    public enum VideoCodec 
    {
        H264 = 0,
        HEVC = 1,
        MPEG4 = 2,
        VP8 = 3,
        VP9 = 4
    }
}