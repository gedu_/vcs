using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using vcs.Enums;
using vcs.ConfigModels;

namespace vcs.Helpers
{
    public static class ConvertHelper
    {
        public static async Task CheckQueue (IServiceProvider services)
        {
            var db = (DbFactory)services.GetService(typeof(DbFactory));
            var loggerFactory = (ILoggerFactory)services.GetService(typeof(ILoggerFactory));
            var logger = loggerFactory.CreateLogger("ConvertHelper");
            var settings = ((IOptions<Settings>)services.GetService(typeof(IOptions<Settings>))).Value;

            var item = await db.Connection.QueryFirstOrDefaultAsync<Conversion>(
                "select c.*, COUNT(s.status) AS Count from `conversions` AS c left join `conversions` as s on s.Status = @StartedStatus WHERE c.status = @NotStartedStatus;",
                new {
                    StartedStatus = ConversionStatus.Started,
                    NotStartedStatus = ConversionStatus.NotStarted
                }
            );

            // No file to convert or conversion already in progress
            if (string.IsNullOrEmpty(item.Id) || item.Count > 0)
            {
                return;
            }

            await db.Connection.ExecuteAsync("update `conversions` SET `Status` = @Status WHERE `Id` = @Id;", 
                new { 
                    Status = ConversionStatus.Started, 
                    Id = item.Id
                }
            );

            logger.LogInformation($"Starting conversion for {item.Id}");
            
            var convertSucceeded = await Convert(db, item, settings, logger);

            if (convertSucceeded)
            {
                logger.LogInformation($"Succesfully converted {item.Id}");

                await db.Connection.ExecuteAsync("update `conversions` SET `Status` = @Status WHERE `Id` = @Id;", 
                    new { 
                        Status = ConversionStatus.Finished, 
                        Id = item.Id
                    }
                );
            } 
            else 
            {
                logger.LogInformation($"Failed to convert {item.Id}");

                await db.Connection.ExecuteAsync("update `conversions` SET `Status` = @Status WHERE `Id` = @Id;", 
                    new { 
                        Status = ConversionStatus.Failed, 
                        Id = item.Id
                    }
                );
            }
        }

        private static string getVideoEncoderSettings(VideoCodec codec)
        {
            switch (codec)
            {
                case VideoCodec.H264:
                    return "-preset slow -crf 23";
                case VideoCodec.HEVC:
                    return "-preset medium -crf 28";
                case VideoCodec.MPEG4:
                    return "-q:v 5";
                case VideoCodec.VP8:
                    return "-crf 10 -b:v 1M";
                case VideoCodec.VP9:
                    return "-crf 31 -b:v 0";
                default:
                    return "";
            }
        }

        private static async Task<bool> Convert (DbFactory db, Conversion item, Settings settings, ILogger logger)
        {
            var file = await db.Connection.QueryFirstOrDefaultAsync<File>(
                "select * from `files` WHERE `id` = @Id",
                new {
                    Id = item.InputFileId
                }
            ); 

            var result = new List<Guid>();

            (bool hasFreeSpace, long freeSpace, long requiredSpace) = getDiskStatus(settings.OutputPath, new System.IO.FileInfo(file.Path).Length);

            if (!hasFreeSpace)
            {
                logger.LogError($"Not enough disk space to convert file. Free: {freeSpace}, Required approx: {requiredSpace}");
                return false;
            }

            var stdout = String.Empty;
                      
            var videoCodec = item.VideoCodec.ToString().ToLower();
            var audioCodec = item.AudioCodec.ToString().ToLower();
            var videoEncoderSettings = getVideoEncoderSettings(item.VideoCodec);            
            (string containerExtension, string container) = getContainer(item.VideoCodec, item.AudioCodec);

            var subtitleFiles = await GetSubtitleFiles(logger, settings, db, file, item);

            if (!subtitleFiles) 
            {
                return false;
            }

            var audioFiles = await GetAudioFiles(logger, settings, db, file, item);

            if (!audioFiles.Item1) 
            {
                return false;
            }

            try 
            {
                for (var i = 0; i < audioFiles.Item2.Count; i++)
                {
                    var id = Guid.NewGuid();
                    var procInfo = new ProcessStartInfo();
                    procInfo.CreateNoWindow = true;
                    procInfo.UseShellExecute = false;
                    procInfo.FileName = settings.FFmpegExecutable;
                    procInfo.RedirectStandardOutput = true;

                    procInfo.Arguments = $"-i {file.Path} -c:v {videoCodec} -c:a {audioCodec} {videoEncoderSettings} -map 0:v:0 -map 0:a:{i} -max_muxing_queue_size 99999 -f {container} " +
                        $"{Path.Combine(settings.OutputPath, id.ToString())}";             
                    
                    using (var proc = Process.Start(procInfo))
                    {
                        stdout = await proc.StandardOutput.ReadToEndAsync();
                        proc.WaitForExit();

                        if (proc.ExitCode == 0) 
                        {
                            var postfix = "_" + audioFiles.Item2[i];
                            var name = Path.HasExtension(item.Filename) ? Path.GetFileNameWithoutExtension(item.Filename) + postfix  + "." + containerExtension : 
                                item.Filename + postfix + "." + containerExtension;
                            var path = Path.Combine(settings.OutputPath, id.ToString());

                            await db.Connection.ExecuteAsync("insert into `files` (id, name, path, conversionId, language) VALUES (@Id, @Name, @Path, @conversionId, @Language);", 
                                new { 
                                    Id = id,
                                    Name = name,
                                    Path = path,
                                    ConversionId = item.Id,
                                    Language = audioFiles.Item2[i]
                                }
                            );

                            // item.Filename = name;

                            result.Add(id);
                        } else {
                            logger.LogError(stdout);

                            return false;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                logger.LogError(e, "Failed to run proccess");
                return false;
            }

            return true;
        }

        public static Tuple<bool, long, long> getDiskStatus (string path, long size)
        {
            var outputDrive = Path.GetPathRoot(path);
            long freeSpace = 0;

            foreach (var drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady && drive.Name == outputDrive) 
                {
                    freeSpace = drive.TotalFreeSpace;
                }
            }

            if (size >= freeSpace)
            {
                return new Tuple<bool, long, long>(false, freeSpace, size);
            }

            return new Tuple<bool, long, long>(true, freeSpace, size);
        }

        private static async Task<bool> GetSubtitleFiles (ILogger logger, Settings settings, DbFactory db, File file, Conversion item) {
            var stdout = String.Empty;

            var procInfo = new ProcessStartInfo();
            procInfo.CreateNoWindow = true;
            procInfo.UseShellExecute = false;
            procInfo.FileName = settings.FFprobeExecutable;
            procInfo.RedirectStandardOutput = true;
            procInfo.Arguments = $"-i {file.Path} -select_streams s -show_entries stream=index:stream_tags=language -v quiet -of csv=p=0";            
            
            try {
                using (var proc = Process.Start(procInfo))
                {
                    stdout = await proc.StandardOutput.ReadToEndAsync();
                    proc.WaitForExit();

                    var subtitles = stdout.Split(Environment.NewLine).Where(x => x.Length > 0).ToList();

                    if (proc.ExitCode == 0 && (subtitles.Count - 1) > 0) 
                    {
                        return await ExtractSubtitleFiles(logger, settings, db, file, item, subtitles);
                    }

                    return true;
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, "Failed to run proccess");

                return false;
            }
        }

        private static async Task<bool> ExtractSubtitleFiles (ILogger logger, Settings settings, DbFactory db, File file, Conversion item, List<string> subtitles) {
            var result = true;

            for(var i = 0; i < subtitles.Count; i++) {
                var stdout = String.Empty;
                var id = Guid.NewGuid();

                var lang = subtitles[i].Split(",");

                var procInfo = new ProcessStartInfo();
                procInfo.CreateNoWindow = true;
                procInfo.UseShellExecute = false;
                procInfo.FileName = settings.FFmpegExecutable;
                procInfo.RedirectStandardOutput = true;
                procInfo.Arguments = $"-i {file.Path} -map 0:s:{i} -f webvtt {Path.Combine(settings.OutputPath, id.ToString())}";            
                
                try {
                    using (var proc = Process.Start(procInfo))
                    {
                        stdout = await proc.StandardOutput.ReadToEndAsync();
                        proc.WaitForExit();

                        if (proc.ExitCode == 0) 
                        {
                            var name = Path.HasExtension(item.Filename) ? Path.GetFileNameWithoutExtension(item.Filename) + $"_{i}.vtt" : 
                                item.Filename + $"_{i}.vtt";
                            var path = Path.Combine(settings.OutputPath, id.ToString());

                            await db.Connection.ExecuteAsync("insert into `files` (id, name, path) VALUES (@Id, @Name, @Path);", 
                                new { 
                                    Id = id,
                                    Name = name,
                                    Path = path
                                }
                            );

                            await db.Connection.ExecuteAsync("insert into `subtitles` (videoId, fileId, language) VALUES (@VideoId, @FileId, @Language);", 
                                new { 
                                    VideoId = file.Id,
                                    FileId = id,
                                    Language = lang[1]
                                }
                            );
                        } else {
                            logger.LogError(stdout);
                            result = false;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.LogError(e, "Failed to run proccess");
                    result = false;
                    break;
                }
            }

            return result;
        }

        private static async Task<Tuple<bool, List<string>>> GetAudioFiles (ILogger logger, Settings settings, DbFactory db, File file, Conversion item) {
            var stdout = String.Empty;

            var procInfo = new ProcessStartInfo();
            procInfo.CreateNoWindow = true;
            procInfo.UseShellExecute = false;
            procInfo.FileName = settings.FFprobeExecutable;
            procInfo.RedirectStandardOutput = true;
            procInfo.Arguments = $"-i {file.Path} -select_streams a -show_entries stream_tags=language -v quiet -of csv=p=0";            
            
            try {
                using (var proc = Process.Start(procInfo))
                {
                    stdout = await proc.StandardOutput.ReadToEndAsync();
                    proc.WaitForExit();

                    var audios = stdout.Split(Environment.NewLine).Where(x => x.Length > 0).ToList();

                    if (audios.Count == 0)
                    {
                        logger.LogInformation($"[GetAudioFiles] File has no language defined, pretend its english");
                        audios.Add("eng");
                    }

                    return new Tuple<bool, List<string>>(true, audios);
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, "Failed to run proccess");

                return new Tuple<bool, List<string>>(false, null);
            }
        }

        private static Tuple<string, string> getContainer (VideoCodec videoCodec, AudioCodec audioCodec)
        {
            var videoContainers = new Dictionary<VideoCodec, List<Container>>() {
                { VideoCodec.H264, new List<Container> { Container.AVI, Container.MKV, Container.MP4 } },
                { VideoCodec.HEVC, new List<Container> { Container.MKV, Container.MP4 }},
                { VideoCodec.MPEG4, new List<Container> { Container.AVI, Container.MKV, Container.MP4 }},
                { VideoCodec.VP8, new List<Container> { Container.AVI, Container.MKV, Container.MP4, Container.WEBM }},
                { VideoCodec.VP9, new List<Container> { Container.MKV, Container.MP4, Container.WEBM }}
            };

             var audioContainers = new Dictionary<AudioCodec, List<Container>>() {
                { AudioCodec.AAC, new List<Container> { Container.AVI, Container.MKV, Container.MP4, Container.WEBM } },
                { AudioCodec.AC3, new List<Container> { Container.AVI, Container.MKV, Container.MP4, Container.WEBM }},
                { AudioCodec.DTS, new List<Container> { Container.AVI, Container.MKV, Container.MP4, Container.WEBM }},
                { AudioCodec.MP3, new List<Container> { Container.AVI, Container.MKV, Container.MP4, Container.WEBM }},
                { AudioCodec.OPUS, new List<Container> { Container.MKV, Container.MP4, Container.WEBM }},
                { AudioCodec.VORBIS, new List<Container> { Container.MKV, Container.WEBM }}
            };

            var containers = videoContainers[videoCodec].Intersect(audioContainers[audioCodec]);

            if (containers.Any(x => x == Container.MP4))
            {
                return new Tuple<string, string>("mp4", "mp4");
            }
            else if (containers.Any(x => x == Container.WEBM))
            {
                return new Tuple<string, string>("webm", "webm");
            }
            else if (containers.Any(x => x == Container.MKV))
            {
                return new Tuple<string, string>("mkv", "matroska");
            }
            
            return new Tuple<string, string>("avi", "avi");
        }
    }
}