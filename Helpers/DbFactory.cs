using System;
using System.Data.SQLite;
using System.IO;
using Dapper;
using Microsoft.Extensions.Options;
using vcs.ConfigModels;

namespace vcs.Helpers
{
    public class DbFactory : IDisposable
    {
        public readonly SQLiteConnection Connection;

        public DbFactory (IOptions<Settings> settings) 
        {
            Connection = new SQLiteConnection(settings.Value.ConnectionString);
            Connection.Open();
            EnsureCreated();
        }

        public void EnsureCreated ()
        {
            var sqlCommands = System.IO.File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), "db.sql"));

            Connection.Execute(string.Join(Environment.NewLine, sqlCommands));
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Connection.Close();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion

    }
}