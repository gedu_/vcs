using System.Linq;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using vcs.Attributes;

namespace vcs.Filters
{
    public class FileFilter : IOperationFilter
    {
        void IOperationFilter.Apply(Operation operation, OperationFilterContext context)
        {
            if (context.MethodInfo.GetCustomAttributes(true).Any(x => x.GetType() == typeof(DisableFormValueModelBindingAttribute))) 
            {
                operation.Consumes.Add("multipart/form-data");

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "file",
                    In = "formData",
                    Description = "File to upload",
                    Required = true,
                    Type = "file"
                });
            }
        }
    }
}