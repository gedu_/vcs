# Video converting service

Video converting service - a service to convert video files from any arbitrary format to any of predefined formats, which are compatible with modern browsers. All communication is done through a REST API. Utilises FFMpeg for conversion.

## Requirements

+ Mysql/MariaDB RDBMS

+ FFMpeg

+ Dotnet Core 2.1+

## Usage Example

Typical usage case:

1. User uploads video file to the service
2. User queues a conversion in the service
3. Service converts input file
4. User downloads coverted file
5. User deletes input and output files from the service

Service utilises ffmpeg to convert videos

## How to use

1. Upload a file using `POST /file` route. For example: `curl -v -F 'file=@filename.mkv' http://localhost:3005/file`
2. Use `GET /audioCodec` to get available audio codecs
3. Use `GET /videoCodec` to get available video codecs
4. Use `GET /conversionStatus` to get status enumeration for conversion
5. Queue a conversion using `POST /conversion` with body (If filename contains extension it will be stripped and changed with container extension)

```json
{
    "inputFileId": "uploaded file id",
    "audioCodec": "audio codec",
    "videoCodec": "video codec",
    "filename": "file name"
}
```

6. Periodically issue `GET /conversion/:id` with the id returned from `POST /conversion` to check status
7. When conversion is done issue `GET /file/:outputFileId`
8. Clean up by issuing `DELETE /file/:inputFileId` and `DELETE /file/:outputFileId`

## How to create a service (Windows)

1. Build the source using `dotnet publish -c release`
2. Open Command Prompt/Powershell in administrator mode and issue these commands (note that whitespaces before = sign are intentional and word service is mandatory after vcs.exe):

`sc.exe create vcs binPath= "C:\path\to\service\vcs.exe service"  start= auto`

`sc.exe failure vcs reset= 60 actions= restart/1000/restart/1000/restart/1000`

## How to create a service (Linux)

1. Open vcs.csproj with any editor and change RuntimeIdentifier with generic linux-x64 or select one from [here](https://docs.microsoft.com/en-us/dotnet/core/rid-catalog)
2. Run `dotnet restore`
3. Run `dotnet publish -c release`
4. Change User, WorkingDirectory, ExecStart variables in vcs.service
5. Copy vcs.service to appropriate folder in your distribution, for example: `/etc/systemd/system/multi-user.target.wants/`
6. `systemd enable vcs`
7. `systemd start vcs`

## API

API can be explored using swagger at the parent route.

API has one route that is not mentioned here `GET /status`. It is used to check availability of the service and returns 200 status code with empty response.

## Container

Container is automatically selected depending on video and audio codecs, priority being:

1. MP4
2. WEBM
3. MKV (Matroska)
4. AVI

## Settings

Settings object contains such properties:

1. ConnectionString (string) => MySql connection string of format "host=host;user id=user;password=pass;database=db;"
2. Connectivity (object)
   1. OutPort (int) => Port used by the api
   2. BindLocalhost (bool) => Whether api should be binded to localhost
   3. BindExternal (bool) => Whether api should be binded to all ips on the machine
3. InputPath (string) => A valid location in the filesystem where to store uploaded files
4. OutputPath (string) => A valid location in the filesystem where to store converted files
5. TimerInterval (long) => Conversion queue execution timer (ms)
6. FFmpegExecutable (string) => Path to ffmpeg executable, containing executable itself