﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dapper;
using vcs.Enums;
using vcs.DTOs;

namespace vcs.Controllers
{
    [Route("[controller]")]
    public class ConversionStatusController : Controller
    {
        [HttpGet]
        [ProducesResponseType(typeof(List<ConversionStatusDTO>), 200)]
        public IActionResult Get ()
        {
            var statuses = Enum.GetValues(typeof(ConversionStatus)).Cast<ConversionStatus>().ToList().Select(x => new ConversionStatusDTO {
                Code = (int)x,
                Name = x.ToString()
            });

            return Ok(statuses);
        }
    }
}
