﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dapper;
using vcs.DTOs;
using vcs.Enums;
using vcs.Helpers;
using System.IO;

namespace vcs.Controllers
{
    [Route("[controller]")]
    public class ConvertController : Controller
    {
        private DbFactory _db;

        public ConvertController (DbFactory db)
        {
            _db = db;
        }

        [HttpGet("{id:Guid}")]
        [ProducesResponseType(typeof(ConversionDTO), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get (Guid id)
        {
            var conversions = await _db.Connection.QueryAsync<Conversion>(
                $@"select c.id, f.name as filename, c.inputFileId, c.`status`, c.videoCodec, c.audioCodec, 
                    f.id as outputFileId, f.language as audioLanguage, s.fileId as subtitleId, s.`language` as subtitleLanguage 
                    from `conversions` c left join `files` f on f.conversionId = c.id left join `subtitles` s on c.inputFileId = s.videoId WHERE c.id = @Id", 
                    new { Id = id.ToString() });
            
            if (conversions == null || conversions.Count() == 0) 
            {
                return BadRequest();
            }

            var subtitles = conversions.GroupBy(x => x.SubtitleId)
                .Select(x => new FileDTO { Id = string.IsNullOrEmpty(x.Key) ? Guid.Empty : Guid.Parse(x.Key), Language = x.First().SubtitleLanguage })
                .Where(x => x.Id != Guid.Empty)
                .ToList();

            var outputFiles = conversions.Where(x => !string.IsNullOrEmpty(x.OutputFileId))
                .GroupBy(x => x.OutputFileId)
                .Select(x => new FileDTO { Id = string.IsNullOrEmpty(x.Key) ? Guid.Empty : Guid.Parse(x.Key), Language = x.First().AudioLanguage, Filename = x.First().Filename })
                .ToList();

            return Ok(new ConversionDTO 
                {
                    Id = conversions.First().Id,
                    InputFileId = conversions.First().InputFileId,
                    OutputFiles = outputFiles,
                    Subtitles = subtitles,
                    Status = conversions.First().Status.ToString(),
                    VideoCodec = conversions.First().VideoCodec.ToString(),
                    AudioCodec = conversions.First().AudioCodec.ToString(),
                    StatusCode = conversions.First().Status
                }
            );
        }

        [HttpPost]
        [ProducesResponseType(typeof(Guid), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Convert ([FromBody] ConversionInputDTO input)
        {
            if (!Enum.IsDefined(typeof(AudioCodec), input.AudioCodec))
            {
                return BadRequest("Unknown AudioCodec");
            }

            if (!Enum.IsDefined(typeof(VideoCodec), input.VideoCodec))
            {
                return BadRequest("Unknown VideoCodec");
            }

            if (input.Filename.Length == 0) 
            {
                return BadRequest("Filename cannot be empty");
            }

            if (Path.HasExtension(input.Filename) && input.Filename.Length >= 255)
            {
                return BadRequest("Filename too long. Limit 255 symbols with extension");
            }
            else if (input.Filename.Length >= 252)
            {
                return BadRequest("Filename too long. Limit 251 symbol without extension");
            }

            var file = await _db.Connection.QueryFirstOrDefaultAsync($"select * from `files` WHERE `id` = @Id", new { Id = input.InputFileId });

            if (file == null) 
            {
                return BadRequest("Input file not found");
            }

            var id = Guid.NewGuid();

            var conversion = await _db.Connection.ExecuteAsync($"insert into `conversions` (id, filename, inputfileid, status, videocodec, audiocodec) VALUES (@Id, @Filename, @InputFileId, @Status, @VideoCodec, @AudioCodec)", 
                new { 
                    Id = id,
                    Filename = input.Filename,
                    InputFileId = input.InputFileId,
                    Status = ConversionStatus.NotStarted,
                    VideoCodec = Enum.Parse(typeof(VideoCodec), input.VideoCodec),
                    AudioCodec = Enum.Parse(typeof(AudioCodec), input.AudioCodec)
                });

            return Created($"convert/{id}", id);
        }
    }
}
