﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dapper;
using vcs.Enums;

namespace vcs.Controllers
{
    [Route("[controller]")]
    public class VideoCodecController : Controller
    {
        [HttpGet]
        [ProducesResponseType(typeof(List<string>), 200)]
        public IActionResult Get ()
        {
            var codecs = Enum.GetValues(typeof(VideoCodec)).Cast<VideoCodec>().ToList().Select(x => x.ToString());

            return Ok(codecs);
        }
    }
}
