using Microsoft.AspNetCore.Mvc;

namespace vcs.Controllers
{
    [Route("[controller]")]
    public class StatusController : Controller
    {
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Get ()
        {
            return Ok();
        }
    }
}