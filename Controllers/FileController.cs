﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Net.Http.Headers;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using MimeTypes;
using System.Text;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Http;
using System.Globalization;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using vcs.ConfigModels;
using vcs.Helpers;
using vcs.Attributes;

namespace vcs.Controllers
{
    [Route("[controller]")]
    public class FileController : Controller
    {
        private DbFactory _db;
        private ILogger _logger;
        private FormOptions _defaultFormOptions;
        private Settings _settings;

        public FileController (DbFactory db, ILoggerFactory loggerFactory, IOptions<Settings> settings)
        {
            _db = db;
            _logger = loggerFactory.CreateLogger("File");
            _defaultFormOptions = new FormOptions();
            _settings = settings.Value;
        }
        
        [DisableFormValueModelBinding]
        [HttpPost]
        [DisableRequestSizeLimit]
        [ProducesResponseType(400)]
        [ProducesResponseType(typeof(Guid), 201)]
        public async Task<IActionResult> Upload ()
        {
            try {
                if (!MultipartRequestHelper.IsMultipartContentType(Request.ContentType))
                {
                    return BadRequest($"Expected a multipart request, but got {Request.ContentType}");
                }

                string targetFilePath = null;
                var id = Guid.NewGuid();
                ContentDispositionHeaderValue contentDisposition = null;

                var boundary = MultipartRequestHelper.GetBoundary(MediaTypeHeaderValue.Parse(Request.ContentType), 
                    _defaultFormOptions.MultipartBoundaryLengthLimit);

                var reader = new MultipartReader(boundary, HttpContext.Request.Body);
                var section = await reader.ReadNextSectionAsync();

                while (section != null)
                {
                    var hasContentDispositionHeader = ContentDispositionHeaderValue.TryParse(section.ContentDisposition, out contentDisposition);

                    if (hasContentDispositionHeader && MultipartRequestHelper.HasFileContentDisposition(contentDisposition))
                    {
                        (var hasFreeSpace, long freeSpace, long requiredSpace) = 
                            ConvertHelper.getDiskStatus(_settings.InputPath, Request.ContentLength.HasValue ? Request.ContentLength.Value : 0);

                        if (!hasFreeSpace)
                        {
                            _logger.LogError($"Not enough disk space to upload file. Free: {freeSpace}, Required: {requiredSpace}");
                            return StatusCode(413);   
                        }

                        targetFilePath = Path.Combine(_settings.InputPath, id.ToString());

                        using (var targetStream = System.IO.File.Create(targetFilePath))
                        {
                            await section.Body.CopyToAsync(targetStream);

                            _logger.LogInformation($"Uploaded file to '{targetFilePath}'");
                        }
                    }

                    // Drains any remaining section body that has not been consumed and
                    // reads the headers for the next section.
                    section = await reader.ReadNextSectionAsync();
                }

                await _db.Connection.ExecuteAsync($"insert into `files` (id, name, path) VALUES (@Id, @Name, @Path)", 
                    new { 
                        Id = id,
                        Name = HeaderUtilities.RemoveQuotes(contentDisposition.FileName.Value).Value,
                        Path = targetFilePath
                    }
                );

                return Created($"file/{id}", id);
            
            } 
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to upload file");

                return BadRequest();
            }
        }

        [HttpGet("{id:guid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Download (Guid id)
        {
            var file = await _db.Connection.QueryFirstOrDefaultAsync<File>($"select * from `files` WHERE `id` = @Id", new { Id = id });

            if (file == null) 
            {
                return BadRequest();
            }

            var stream = new FileStream(file.Path, FileMode.Open);

            var extension = Path.GetExtension(file.Path);

            return File(stream, MimeTypeMap.GetMimeType(extension), file.Name);
        }

        [HttpDelete("{id:guid}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Delete (Guid id)
        {
            var file = await _db.Connection.QueryFirstOrDefaultAsync<File>($"select * from `files` WHERE `id` = @Id", new { Id = id });

            if (file == null) 
            {
                return BadRequest();
            }

            await _db.Connection.ExecuteAsync($"delete from `conversions` WHERE `inputFileId` = @Id", new { Id = id });            

            System.IO.File.Delete(file.Path);

            await _db.Connection.ExecuteAsync($"delete from `files` WHERE `id` = @Id", new { Id = id });

            return StatusCode(204);
        }
    }
}
