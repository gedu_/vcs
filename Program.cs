﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using vcs.Helpers;

namespace vcs
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var app = new CommandLineApplication();
            var isService = false;

            var service = app.Command("service", config => {
                config.OnExecute(() => {
                    Directory.SetCurrentDirectory(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName));
                    isService = true;
                    return 1;
                });
            });

            app.Execute(args);

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("settings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            var configuration = builder.Build();

            var outPort = Int32.Parse(configuration.GetSection("Connectivity:OutPort").Value);
            var bindLocalhost = Convert.ToBoolean(configuration.GetSection("Connectivity:BindLocalhost").Value);
            var bindExternal = Convert.ToBoolean(configuration.GetSection("Connectivity:BindExternal").Value);

            var hostname = Dns.GetHostName();
            var ips = Dns.GetHostAddressesAsync(hostname).Result;

            var urls = new List<string>();

            if (bindExternal)
            {
                foreach (var ip in ips.Where(x => x.AddressFamily == AddressFamily.InterNetwork))
                {
                    var url = string.Format("http://{0}:{1}/", ip.ToString(), outPort);

                    if (!urls.Contains(url))
                    {
                        urls.Add(url);
                    }
                }
            }

            if (bindLocalhost)
            {
                var localhost = string.Format("http://{0}:{1}/", "localhost", outPort);
                urls.Add(localhost);
            }

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseUrls(urls.ToArray())
                .UseStartup<Startup>()
                .UseLibuv()
                .Build();

            var timerInterval = Double.Parse(configuration.GetSection("TimerInterval").Value);
            var timer = new Timer(timerInterval);

            timer.Elapsed += async (sender, e) => await ConvertHelper.CheckQueue(host.Services);
            timer.Start();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows) && isService)
            {
                host.RunAsService();
            }
            else 
            {
                host.Run();
            }
        }
    }
}
