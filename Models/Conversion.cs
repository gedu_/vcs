using System;
using System.Runtime.Serialization;
using vcs.Enums;

namespace vcs
{
    [DataContract]
    public class Conversion
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Filename { get; set; }
        [DataMember]
        public string InputFileId { get; set; }
        [DataMember]
        public string OutputFileId { get; set; }
        [DataMember]
        public ConversionStatus Status { get; set; }
        [DataMember]
        public VideoCodec VideoCodec { get; set; }
        [DataMember]
        public AudioCodec AudioCodec { get; set; }
        [DataMember]
        public string AudioLanguage { get; set; }
        [DataMember]
        public string SubtitleId { get; set; }
        [DataMember]
        public string SubtitleLanguage { get; set; }

        [IgnoreDataMember]
        public int Count { get; set; }
    }
}