using System;
using System.Runtime.Serialization;

namespace vcs 
{
    [DataContract]
    public class File 
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Path { get; set; }
    }
}