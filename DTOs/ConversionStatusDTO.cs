using System.Runtime.Serialization;

namespace vcs.DTOs
{
    [DataContract]
    public class ConversionStatusDTO
    {
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}