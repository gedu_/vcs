using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using vcs.Enums;

namespace vcs.DTOs
{
    [DataContract]
    public class ConversionDTO
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string InputFileId { get; set; }
        [DataMember]
        public List<FileDTO> OutputFiles { get; set; }
        [DataMember]
        public List<FileDTO> Subtitles { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string VideoCodec { get; set; }
        [DataMember]
        public string AudioCodec { get; set; }
        [DataMember]
        public ConversionStatus StatusCode { get; set; }
    }
}