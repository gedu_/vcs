using System;
using System.Runtime.Serialization;
using vcs.Enums;

namespace vcs.DTOs
{
    [DataContract]
    public class ConversionInputDTO
    {
        [DataMember]
        public Guid InputFileId { get; set; }
        [DataMember]
        public string AudioCodec { get; set; }
        [DataMember]
        public string VideoCodec { get; set; }
        [DataMember]
        public string Filename { get; set; }
    }
}