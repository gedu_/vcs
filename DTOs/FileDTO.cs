using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using vcs.Enums;

namespace vcs.DTOs
{
    [DataContract]
    public class FileDTO
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Filename { get; set; }
        [DataMember]
        public string Language { get; set; }
    }
}