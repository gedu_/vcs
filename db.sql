CREATE TABLE IF NOT EXISTS `files` (
  `id` CHAR(36) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `path` TINYTEXT NOT NULL,
  `conversionId` CHAR(36) NULL,
  `language` CHAR(20) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `subtitles` (
  `videoId` CHAR(36) NOT NULL,
  `fileId` CHAR(36) NOT NULL,
  `language` CHAR(20) NOT NULL);

CREATE TABLE IF NOT EXISTS `conversions` (
  `id` CHAR(36) NOT NULL,
  `filename` VARCHAR(255) NOT NULL,
  `inputFileId` CHAR(36) NOT NULL,
  `status` SMALLINT(1) NOT NULL,
  `videoCodec` SMALLINT(2) NOT NULL,
  `audioCodec` SMALLINT(2) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (inputFileId) REFERENCES files(id));