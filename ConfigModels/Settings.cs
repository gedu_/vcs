namespace vcs.ConfigModels
{
    public class Settings
    {
        public string ConnectionString { get; set; }

        public Connectivity Connectivity { get; set; }

        public string InputPath { get; set; }

        public string OutputPath { get; set; }

        public string FFmpegExecutable { get; set; }

        public string FFprobeExecutable { get; set; }
    }
}