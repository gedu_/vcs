namespace vcs.ConfigModels
{
    public class Connectivity
    {
        public int OutPort { get; set; }

        public bool BindLocalhost { get; set; }

        public bool BindExternal { get; set; } 
    }
}